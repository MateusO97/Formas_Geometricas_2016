#include <iostream>
#include "quadrado.hpp"

Quadrado::Quadrado() {
	setLados(4);
	setLargura(10);
	setAltura(10);
}
Quadrado::Quadrado(float largura) {
	setLados(4);
	setLargura(largura);
	setAltura(largura);
}